package com.example.demo.controller;

import com.example.demo.dto.ReaderDto;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.example.demo.service.IReaderService;

@RestController
@RequestMapping("/readers")
public class ReaderController {

    private final IReaderService _readerService;

    public ReaderController(IReaderService readerService) {
        _readerService = readerService;
    }

    @PostMapping
    public void index(@RequestBody ReaderDto readerDto){
        _readerService.index(readerDto);
    }

}
