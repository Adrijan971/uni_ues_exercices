package com.example.demo.service;

import com.example.demo.dto.ReaderDto;

public interface IReaderService {
    void index(ReaderDto readerDto);
}
