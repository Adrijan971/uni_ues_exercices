package com.example.demo.service;

import com.example.demo.dto.ReaderDto;

import com.example.demo.model.Reader;
import org.springframework.stereotype.Service;
import com.example.demo.repository.IReaderRepository;


@Service
public class ReaderService implements IReaderService {

    private final IReaderRepository _readerRepository;

    public ReaderService(IReaderRepository readerRepository) {
        _readerRepository = readerRepository;
    }

    @Override
    public void index(ReaderDto readerDto) {
        _readerRepository.save(
                new Reader(
                        "123",
                        readerDto.getFirstName(),
                        readerDto.getLastName(),
                        readerDto.getAddress()
                )
        );
    }
}
