package com.example.demo.repository;

import com.example.demo.model.Reader;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IReaderRepository extends ElasticsearchRepository<Reader,String> {
}
