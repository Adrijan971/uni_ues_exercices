package com.example.demo.dto;

import lombok.Data;

@Data
public class ReaderDto {

    private String lastName;

    private String firstName;

    private String address;
}
