package com.example.demo.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

@Setter
@Getter
@AllArgsConstructor
@Document(indexName = "readers")
public class Reader {

    @Id
    private String id;
    private String lastName;
    private String firstName;
    private String address;
}
